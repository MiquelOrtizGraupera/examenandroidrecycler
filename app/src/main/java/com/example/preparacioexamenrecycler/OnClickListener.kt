package com.example.preparacioexamenrecycler

import com.example.preparacioexamenrecycler.dataclass.ItemApiDataClass

interface OnClickListener {
    fun onCLick(item: ItemApiDataClass)
}