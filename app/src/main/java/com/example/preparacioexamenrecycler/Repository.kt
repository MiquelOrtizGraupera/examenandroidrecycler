package com.example.preparacioexamenrecycler

import com.example.preparacioexamenrecycler.api.ApiInterface

class Repository {
    private val apiInterface = ApiInterface.create()
    suspend fun getAllItems() = apiInterface.getData()
}