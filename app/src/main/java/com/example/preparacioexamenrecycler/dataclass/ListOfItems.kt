package com.example.preparacioexamenrecycler.dataclass

data class ListOfItems(
    val items: List<ItemApiDataClass>
)
