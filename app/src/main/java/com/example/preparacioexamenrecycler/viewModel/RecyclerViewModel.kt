package com.example.preparacioexamenrecycler.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.preparacioexamenrecycler.Repository
import com.example.preparacioexamenrecycler.dataclass.ItemApiDataClass
import com.example.preparacioexamenrecycler.dataclass.ListOfItems
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RecyclerViewModel: ViewModel() {
    private val repo = Repository()
    var data = MutableLiveData<ListOfItems>()
    var selectedItem = MutableLiveData<ItemApiDataClass>()

    init {
        fetchData()
    }

    private fun fetchData(){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO){
                repo.getAllItems()
            }
            if(response.isSuccessful){
                data.postValue(response.body())
            }
            else{
                Log.e("Error", response.message())
            }
        }
    }

    fun select(item:ItemApiDataClass){
        selectedItem.postValue(item)
    }
}