package com.example.preparacioexamenrecycler.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.preparacioexamenrecycler.OnClickListener
import com.example.preparacioexamenrecycler.adapter.appAdapter
import com.example.preparacioexamenrecycler.databinding.ActivityMainBinding
import com.example.preparacioexamenrecycler.databinding.FragmentRecyclerBinding
import com.example.preparacioexamenrecycler.dataclass.ItemApiDataClass
import com.example.preparacioexamenrecycler.viewModel.RecyclerViewModel


class RecyclerFragment : Fragment(), OnClickListener {
    private val viewModel: RecyclerViewModel by activityViewModels()
    private lateinit var binding: FragmentRecyclerBinding
    private lateinit var appAdapter: appAdapter
    private lateinit var linearLayout: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentRecyclerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        linearLayout = LinearLayoutManager(context)
    }

    override fun onCLick(item: ItemApiDataClass) {
        viewModel.select(item)

    }
}