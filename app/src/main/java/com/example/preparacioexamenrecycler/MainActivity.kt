package com.example.preparacioexamenrecycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.preparacioexamenrecycler.databinding.ActivityMainBinding
import com.example.preparacioexamenrecycler.fragments.RecyclerFragment

class MainActivity : AppCompatActivity() {
 lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, RecyclerFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }
    }
}