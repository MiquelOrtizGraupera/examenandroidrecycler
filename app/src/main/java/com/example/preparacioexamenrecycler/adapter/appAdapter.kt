package com.example.preparacioexamenrecycler.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.preparacioexamenrecycler.OnClickListener
import com.example.preparacioexamenrecycler.R
import com.example.preparacioexamenrecycler.databinding.ItemRecyclerBinding
import com.example.preparacioexamenrecycler.dataclass.ItemApiDataClass
import com.example.preparacioexamenrecycler.dataclass.ListOfItems

class appAdapter(private val items: ListOfItems, private val listener:OnClickListener): RecyclerView.Adapter<appAdapter.ViewHolder>(){
    inner class ViewHolder(view:View): RecyclerView.ViewHolder(view){
        val binding = ItemRecyclerBinding.bind(view)

        fun setListener(item:ItemApiDataClass){
            binding.root.setOnClickListener {
                listener.onCLick(item)
            }
        }

    }

    private lateinit var context:Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_recycler, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items.items[position]
        with(holder){
            setListener(item)
            binding.itemTextView.text = item.name
            Glide.with(context)
                .load(item.image_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.itemImageView)
        }
    }

    override fun getItemCount(): Int {
        return items.items.size
    }
}